﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Hressurs_testapplikasjon
{
    public class Journalist
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public string OfficePhone { get; set; }

        public string MobilePhone { get; set; }

        public string PrivatePhone { get; set; }

        public string Position { get; set; }

        public string Signature { get; set; }

        public Address Address { get; set; }
    }
   
}
