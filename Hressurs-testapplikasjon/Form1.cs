﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Hressurs_testapplikasjon.PersonService;
using Hressurs_testapplikasjon.UnitService;
using UnitIdentifierType = Hressurs_testapplikasjon.PersonService.UnitIdentifierType;

namespace Hressurs_testapplikasjon
{
    public partial class Form1 : Form
    {
        private readonly PersonClient _personClient = new PersonClient();
        private readonly UnitClient _unitClient = new UnitClient();
        
        public Form1()
        {
            
            InitializeComponent();

            txtServiceOutput.ScrollBars = ScrollBars.Both;
        }

        private void btnGetPersons_Click(object sender, EventArgs e)
        {
            if (_personClient.ClientCredentials != null)
            {
                if (string.IsNullOrEmpty(_personClient.ClientCredentials.UserName.UserName))
                {
                    _personClient.ClientCredentials.UserName.UserName = "thu@ntb.no";
                    _personClient.ClientCredentials.UserName.Password = "vappa30=infotjenester";
                }
            }

            if (_unitClient.ClientCredentials != null)
            {
                if (string.IsNullOrEmpty(_unitClient.ClientCredentials.UserName.UserName))
                {
                    _unitClient.ClientCredentials.UserName.UserName = "thu@ntb.no";
                    _unitClient.ClientCredentials.UserName.Password = "vappa30=infotjenester";
                }
            }


            var unitRequest = new ExportUnitRequest();
            var unitResponse = _unitClient.Export(unitRequest);

            if (!unitResponse.Units.Any())
            {
                // do nothing really
            }

            var sb = new StringBuilder();
            foreach (var unit in unitResponse.Units)
            {
                sb.Append(@"Name: " + unit.Name + Environment.NewLine);
                
                if (unit.Name == null)
                {
                    continue;
                }
                
                var request = new ExportPersonRequest();
                // request.FromUnit.Identifiertype = UnitIdentifierType.Name;
                var ui = new PersonService.UnitIdentifier();
                ui.Identifiertype = UnitIdentifierType.Name;
                ui.Value = unit.Name;
                request.FromUnit = ui;

                var response = _personClient.Export(request);


                if (!response.Persons.Any())
                {
                    return;
                }

                

                var journalists = new List<Journalist>();

                foreach (var person in response.Persons)
                {
                    if (string.IsNullOrEmpty(person.ShortName))
                    {
                        continue;
                    }

                    var journalist = new Journalist
                    {
                        FirstName = person.FirstName,
                        LastName = person.LastName,
                        Signature = person.ShortName.ToLower(),
                        BirthDate = Convert.ToDateTime(person.BirthDate)
                    };


                    journalists.Add(journalist);
                    sb.Append(person.FirstName + @" " + person.LastName + @" (" + person.ShortName.ToUpper() + @")" +
                              Environment.NewLine);

                    if (person.Addresses != null)
                    {
                        foreach (var add in person.Addresses)
                        {
                            sb.Append(@"Type: " + add.Type + Environment.NewLine);
                            sb.Append(@"StreetName1: " + add.StreetName1 + Environment.NewLine);
                            sb.Append(@"StreetName2: " + add.StreetName2 + Environment.NewLine);
                            sb.Append(@"StreetName3: " + add.StreetName3 + Environment.NewLine);
                            sb.Append(@"PostalArea: " + add.PostalArea + Environment.NewLine);
                            sb.Append(@"ZipCode: " + add.ZipCode + Environment.NewLine);
                            sb.Append(@"CountryCode: " + add.CountryCode + Environment.NewLine);
                            sb.Append(Environment.NewLine + Environment.NewLine);
                        }
                    }

                    if (person.Phones != null)
                    {
                        foreach (var add in person.Phones)
                        {
                            sb.Append(@"Type: " + add.Type + Environment.NewLine);
                            sb.Append(@"StreetName1: " + add.Number + Environment.NewLine);
                            sb.Append(Environment.NewLine + Environment.NewLine);
                        }
                    }
                }

                sb.Append(@"Totalt: " + journalists.Count + Environment.NewLine);
                sb.Append(Environment.NewLine);
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                var serializer = new XmlSerializer(typeof (List<Journalist>));
                
                StringWriter stringWriter = new StringWriter();
                XmlWriter writer = XmlWriter.Create(stringWriter);

                serializer.Serialize(writer, journalists, namespaces);

                var xml = stringWriter.ToString();
                
                // writer.WriteElementString("TimeStamp", DateTime.Now.ToLongDateString());
                
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                var fragment = xmlDoc.CreateDocumentFragment();
                var timestamp = xmlDoc.CreateElement("TimeStamp");
                timestamp.SetAttribute("value", DateTime.Now.ToString());
                var textnode = xmlDoc.CreateTextNode(DateTime.Now.ToLongDateString());
                timestamp.AppendChild(textnode);
                fragment.AppendChild(timestamp);
                xmlDoc.LastChild.AppendChild(fragment);
                // <timestamp>24.04.2013 06:10:00</timestamp>
                
                // xmlDoc.AppendChild(timestamp);
                

                var filename = unit.Name.Replace(@"/", "_");

                xmlDoc.Save(@"c:\utvikling\NTB\" + filename + "_test.xml");
            }
            txtServiceOutput.Text += Environment.NewLine;
            txtServiceOutput.Text += sb.ToString();
        }
    }
}
