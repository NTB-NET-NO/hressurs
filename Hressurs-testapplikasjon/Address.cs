﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hressurs_testapplikasjon
{
    public class Address
    {
        public string Street { get; set; }

        public string PostalNumber { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
