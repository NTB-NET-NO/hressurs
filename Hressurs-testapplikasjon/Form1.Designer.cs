﻿namespace Hressurs_testapplikasjon
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetPersons = new System.Windows.Forms.Button();
            this.txtServiceOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnGetPersons
            // 
            this.btnGetPersons.Location = new System.Drawing.Point(3, 361);
            this.btnGetPersons.Name = "btnGetPersons";
            this.btnGetPersons.Size = new System.Drawing.Size(75, 23);
            this.btnGetPersons.TabIndex = 0;
            this.btnGetPersons.Text = "Get Persons";
            this.btnGetPersons.UseVisualStyleBackColor = true;
            this.btnGetPersons.Click += new System.EventHandler(this.btnGetPersons_Click);
            // 
            // txtServiceOutput
            // 
            this.txtServiceOutput.Location = new System.Drawing.Point(13, 13);
            this.txtServiceOutput.Multiline = true;
            this.txtServiceOutput.Name = "txtServiceOutput";
            this.txtServiceOutput.Size = new System.Drawing.Size(1008, 259);
            this.txtServiceOutput.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 396);
            this.Controls.Add(this.txtServiceOutput);
            this.Controls.Add(this.btnGetPersons);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetPersons;
        private System.Windows.Forms.TextBox txtServiceOutput;
    }
}

